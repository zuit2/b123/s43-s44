let nameCourse = document.querySelector("#name-input");
let descriptionCourse = document.querySelector("#description-input");
let priceCourse = document.querySelector("#price-input");
let token = localStorage.getItem('token');

document.querySelector('#form-add-course').addEventListener('submit', (e) =>{

	e.preventDefault();

	console.log(nameCourse.value);
	console.log(descriptionCourse.value);
	console.log(priceCourse.value);

	fetch('http://localhost:4000/courses/',{

		method: 'POST',
		headers: {
			'Authorization': `Bearer ${token}`,
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({

			name: nameCourse.value,
			description: descriptionCourse.value,
			price: priceCourse.value
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);
		
	})
})