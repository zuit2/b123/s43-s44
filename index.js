let coursesDiv = document.querySelector("#courses-div");
console.log(coursesDiv);
// .innerHTML = property of an element which contains all of the children (other elements or text content) as a string.
/*console.log(coursesDiv.innerHTML);

let mainDiv = document.querySelector("#main-div");
console.log(mainDiv.innerHTML);

mainDiv.innerHTML = "<h1> Batch 123 is awesome</h1>" 
mainDiv.innerHTML += "<p>Full Stack Developers</p>" 
mainDiv.innerHTML = mainDiv.innerHTML + "<p>I am Efren Rodriguez</p>" */



// fetch() is a javascript method which allows to pass or create a request to an api
// syntax fetch(<requestURL>)
//.then() allows us to handle/process the result of a previous function
// .then(res => res.json()) - it handles/process the servers response into a proper JS object.
fetch('http://localhost:4000/courses/getActiveCourses')
.then(res => res.json())
.then(data => {
	// console.log(data);
	let courseCards = "";
	data.forEach((course)=>{

		console.log(course);
		//add a div for each item in our array:
		courseCards += `

			<div class="card">
				<h4>${course.name}</h4>
				<p>${course.description}<p>
				<span>${course.price}</span>
			</div>

		`

	});

	console.log(courseCards);
	coursesDiv.innerHTML = courseCards;
})