// get user details

let token = localStorage.getItem('token');
// console.log(token);

let profileName = document.querySelector('#profile-name');
let profileEmail = document.querySelector('#profile-email');
let profileMobile = document.querySelector('#profile-mobile');


fetch('http://localhost:4000/users/getUserDetails',{

	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	console.log(data);

	profileName.innerHTML = `Hello, ${data.firstName} ${data.lastName}`;
	profileEmail.innerHTML = `Email: ${data.email}`;
	profileMobile.innerHTML = `Mobile: ${data.mobileNo}`;
	
	


})